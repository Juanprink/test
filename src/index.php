<?php
class Contact {
    private $name;
    private $age;
    private $dni;
    private $source;
    private $tags;
    private $phone;
    private $external_id;

    public function __construct($data) {
        $this->name = $data[0];
        $this->age = $data[1];
        $this->dni = $data[2];
        $this->source = $data[3];
        $this->tags = $data[4];
        $this->phone = $data[5];
        $this->external_id = $data[6];
    }

    public function getName() {
        return $this->name;
    }

    public function getAge() {
        return $this->age;
    }

    public function getDni() {
        return $this->dni;
    }

    public function getSource() {
        return $this->source;
    }

    public function getTags() {
        return $this->tags;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getExternalId() {
        return $this->external_id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setAge($age) {
        $this->age = $age;
    }

    public function setDni($dni) {
        $this->dni = $dni;
    }

    public function setSource($source) {
        $this->source = $source;
    }

    public function setTags($tags) {
        $this->tags = $tags;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setExternalId($external_id) {
        $this->external_id = $external_id;
    }
    public function getContact(){
        return array(
            'name' => $this->getName(),
            'age' => $this->getAge(),
            'dni' => $this->getDni(),
            'source' => $this->getSource(),
            'tags' => $this->getTags(),
            'phone' => $this->getPhone(),
            'external_id' => $this->getExternalId()
          );
    }
}

class ContactManager {
    public $contacts = array();

    public function addContact(Contact $contact) {
         // Comprobar si ya existe un contacto con las mismas características
        foreach ($this->contacts as $existingContact) {
            if ($existingContact->getExternalId() === $contact->getExternalId()) {
                return false;

            }
        }
        $existingContact = $this->getContactByDetails($contact->getAge(), $contact->getDni(), $contact->getPhone());
            if ($existingContact) {
                $this->combineContacts($existingContact, $contact);
                return true;
            } else {
                $this->contacts[] = $contact;
                return true;
            }

    }
    private function combineContacts(Contact $existingContact, Contact $newContact) {
        $existingContact->setExternalId($newContact->getExternalId());
        $existingContact->setTags($existingContact->getTags()."|".$newContact->getTags());
    }
    private function getContactByDetails($age, $dni, $phone) {
        foreach ($this->contacts as $contact) {
            if ($contact->getAge() == $age && $contact->getDni() == $dni && $contact->getPhone() == $phone) {
                return $contact;
            }
        }
        return false;
    }

    public function getContacts() {
        return $this->contacts;
    }
}

class ContactValidator {
    public static function validateContact($contact) {
 
        $errors = [];
        // Validating name
        if(!preg_match('/^[a-zA-Z]+ [a-zA-Z]+$/', $contact->getName())){
            if(!preg_match('/^[a-zA-Z]+ [a-zA-Z]\.[ a-zA-Z]+$/', $contact->getName())){
                $errors['name'] = 'Nombre no cumple con el formato requerido.';
            }
        }

        // Validating age
        if (!preg_match('/^\d{1,3}$/',  $contact->getAge()) || $contact->getAge() < 1 || $contact->getAge() > 125) {
            $errors['age'] = 'Edad no cumple con el formato requerido.';
        }

        // Validating DNI
        if (!preg_match('/^\d{11}$/', $contact->getDni())) {
            $errors['dni'] = 'DNI no cumple con el formato requerido.';
        }

        // Validating source
        if (!in_array(strtolower($contact->getSource()), ['google forms', 'facebook leads', 'email response', 'manual registration'])) {
            $errors['source'] = 'Fuente no es válida.';
        }

        // Validating tags
        $tags = explode("|", $contact->getTags());
        foreach ($tags as $key => $value) {
            if (!preg_match('/^[a-zA-Z0-9_]+$/', $value)){
                $errors['tag'] = 'Tags no cumplen con el formato requerido.';
            }
        }


        // Validating phone number
        $cleaned_number = preg_replace("/[\s-]/", "", $contact->getPhone());
        if(strlen($cleaned_number)> 11){
            if (!preg_match("/^\+54\d{11}$/", $cleaned_number)) {
                $errors['phone'] = 'Teléfono no cumple con el formato requerido.';
            }
        }

        // Validating external ID
        if (!preg_match('/^\d{5}$/', $contact->getExternalId()) || $contact->getExternalId() < 10000 || $contact->getExternalId() > 99999) {
            $errors['externalId'] = 'ID externo no cumple con el formato requerido.';
        }

        return $errors;
    }
}

class CSVReader {
    private $filePath;

    public function __construct($filePath) {
        $this->filePath = $filePath;
    }

    public function readCSV() {
        $contacts = array();
        $header = null;
        if (($handle = fopen($this->filePath, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $contacts[] = new Contact($row);
                    
                }
            }
            fclose($handle);
        }
        return $contacts;
    }
    public function writeCSV($contacts) {
        if (($handle = fopen($this->filePath, 'w')) !== false) {
            foreach ($contacts as $contact) {
                fputcsv($handle, $contact->getContact());
            }
            fclose($handle);
            return true;
        }
        return false;
    }
}
$errorData = array();
$reader = new CSVReader('result.csv');
$register = $reader->readCSV();
$validator = new ContactValidator();
$contacts = new ContactManager();
foreach ($register as $key => $value) {
  //validaciones y errores
   $error = $validator->validateContact($value);
    if(empty($error)){
        $contacts->addContact($value);
    }else{
        $errorData[] = [$key => $error];
     
    }
}
$json = json_encode($errorData);
$fp = fopen('errors.json', 'w');
fwrite($fp, $json);
fclose($fp);

$newCSV = new CSVReader("new_result.csv");
$newCSV->writeCSV($contacts->getContacts());

