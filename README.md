# Instrucciones para utilizar PHP y Postgres con Docker y Docker Compose

Este proyecto proporciona una forma de ejecutar PHP y Postgres en contenedores de Docker utilizando Docker Compose.

## Prerequisitos
Antes de comenzar, asegúrese de tener lo siguiente instalado en su sistema:

[Docker](https://docs.docker.com/engine/install/)

[Docker Compose](https://docs.docker.com/compose/install/)
## Clonar el repositorio
Clone este repositorio en su sistema local:

```git clone https://gitlab.com/Juanprink/test.git```

## Construir y ejecutar los contenedores
Navegue hasta la carpeta del repositorio clonado y ejecute el siguiente comando para construir y ejecutar los contenedores:

```docker-compose up -d```

El comando anterior descargará las imágenes de PHP y Postgres si aún no están en su sistema y las ejecutará como contenedores.

## Acceder a los contenedores

Para acceder a un contenedor, use el siguiente comando:

```docker exec -it nombre-del-contenedor bash```

Si no conoce el nombre del contenedor puede usar el siguiente comando para ver los contenedores activos

```docker ps```

## Ejecutar el script de prueba de conexión
El script de prueba de conexión se encuentra en el directorio compartido "/var/www/test". Para ejecutarlo, siga los siguientes pasos:

1. Acceda al contenedor de PHP utilizando el comando descrito en la sección anterior.
2. Navegue hasta el directorio de scripts:


  ```cd /var/www/test```


3. Ejecute el script de prueba de conexión:

```php index.php``` 

El script debería hacer una consulta a la base de datos Postgres y devolver una respuesta.

Opened database successfully